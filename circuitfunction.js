function CFunction(bits, de) {
	this._bits = new Int32Array(bits);
	this._divideError = de;
}

CFunction.argument = function(argindex) {
	var bits = circuit.argument(argindex);
	return new CFunction(bits, 0);
}

CFunction.constant = function(value) {
	var bits = new Int32Array(32);
	for (var i = 0; i < bits.length; i++) {
		bits[i] = (value << (i ^ 31)) >> 31;
	}
	return new CFunction(bits, 0);
}

CFunction.not = function(x) {
	var bits = new Int32Array(32);
	for (var i = 0; i < bits.length; i++) {
		bits[i] = ~x._bits[i];
	}
	return new CFunction(bits, x._divideError);
}

CFunction.rbit = function(x) {
	var bits = new Int32Array(32);
	for (var i = 0; i < bits.length; i++) {
		bits[i] = x._bits[bits.length - i - 1];
	}
	return new CFunction(bits, x._divideError);
}

CFunction.and = function(x, y) {
	var bits = new Int32Array(32);
	for (var i = 0; i < bits.length; i++) {
		bits[i] = circuit.and(x._bits[i], y._bits[i]);
	}
	return new CFunction(bits, circuit.or(x._divideError, y._divideError));
}

CFunction.or = function(x, y) {
	var bits = new Int32Array(32);
	for (var i = 0; i < bits.length; i++) {
		bits[i] = circuit.or(x._bits[i], y._bits[i]);
	}
	return new CFunction(bits, circuit.or(x._divideError, y._divideError));
}

CFunction.xor = function(x, y) {
	var bits = new Int32Array(32);
	for (var i = 0; i < bits.length; i++) {
		bits[i] = circuit.xor(x._bits[i], y._bits[i]);
	}
	return new CFunction(bits, circuit.or(x._divideError, y._divideError));
}

CFunction.mux = function(x, y, z) {
	var bits = new Int32Array(32);
	for (var i = 0; i < bits.length; i++) {
		bits[i] = circuit.mux(y._bits[i], z._bits[i], x._bits[i]);
	}
	return new CFunction(bits, circuit.or_big(x._divideError, y._divideError, z._divideError));
}

CFunction.add2 = function(x, y) {
	var bits = new Int32Array(32);
	var carry = 0;
	for (var i = 0; i < 32; i++) {
		var a = x._bits[i];
		var b = y._bits[i];
		var ab = circuit.and(a, b);
		var aob = circuit.or(a, b);
		var abc = circuit.and(ab, carry);
		var nabc = circuit.and(~aob, ~carry);
		carry = circuit.or(circuit.and(carry, aob), ab);
		bits[i] = circuit.and(circuit.or(~carry, abc), ~nabc);
	}
	return new CFunction(bits, circuit.or(x._divideError, y._divideError));
}

CFunction.add = function(x, y) {
	let bits = new Int32Array(32);
	let carry = 0;
	for (var i = 0; i < 32; i++) {
		let a = x._bits[i];
		let b = y._bits[i];
		let axb = circuit.xor(a, b);
		bits[i] = circuit.xor(axb, carry);
		carry = circuit.carry(a, b, carry);
	}
	return new CFunction(bits, circuit.or(x._divideError, y._divideError));
}

CFunction.addss = function(x, y) {
	let bits = new Int32Array(32);
	let carry = 0;
	for (let i = 0; i < bits.length; i++) {
		let a = x._bits[i];
		let b = y._bits[i];
		let axb = bdd.xor(a, b, carry);
		bits[i] = circuit.xor(axb, carry);
		carry = circuit.carry(a, b, carry);
	}
	// there is signed overflow iff:
	// the signs of the inputs are equal, and different from the sign of the sum
	let overflow = circuit.and(~circuit.xor(x._bits[31], y._bits[31]), circuit.xor(x._bits[31], bits[31]));
	for (let i = 0; i < bits.length; i++) {
		bits[i] = circuit.mux(overflow, bits[i], x._bits[31] ^ (i === 31 ? 0 : -1));
	}

	return new CFunction(bits, circuit.or(x._divideError, y._divideError));
}

CFunction.subss = function(x, y) {
	return CFunction.not(CFunction.add(CFunction.not(x), y));
}

CFunction.sub = function(x, y) {
	return CFunction.not(CFunction.add(CFunction.not(x), y));
}

CFunction.subus = function(x, y) {
	var bits = new Int32Array(32);
	var borrow = 0;
	for (var i = 0; i < bits.length; i++) {
		bits[i] = circuit.xor(x._bits[i], circuit.xor(y._bits[i], borrow));
		borrow = circuit.carry(~x._bits[i], y._bits[i], borrow);
	}
	for (var i = 0; i < bits.length; i++)
		bits[i] = circuit.and(bits[i], ~borrow);
	
	return new CFunction(bits, circuit.or(x._divideError, y._divideError));
}

CFunction.avg_up = function(x, y) {
	var bits = new Int32Array(32);
	var carry = -1;
	for (var i = 0; i < bits.length; i++) {
		if (i >= 1)
			bits[i - 1] = circuit.xor(circuit.xor(x._bits[i], y._bits[i]), carry);
		carry = circuit.carry(carry, x._bits[i], y._bits[i]);
	}
	bits[31] = carry;
	return new CFunction(bits, circuit.or(x._divideError, y._divideError));
}

CFunction.bzhi = function(x, y) {
	y = CFunction.and(y, CFunction.constant(255));
	var bits = new Int32Array(32);
	for (var i = 0; i < 32; i++) {
		var t = CFunction.le(y, CFunction.constant(i), false);
		bits[i] = circuit.and(~t._bits[0], x._bits[i]);
	}
	return new CFunction(bits, circuit.or(x._divideError, y._divideError));
}

CFunction.hor = function(x) {
	var or = circuit.or_big(x._bits.slice());
	var bits = new Int32Array(32);
	for (var i = 0; i < bits.length; i++)
		bits[i] = or;
	return new CFunction(bits, x._divideError);
}

CFunction.eq = function(x, y) {
	return CFunction.not(CFunction.hor(CFunction.xor(x, y)));
}

CFunction.lt = function(x, y, signed) {
	var borrow = new Int32Array(32);
	borrow[0] = circuit.and(~x._bits[0], y._bits[0]);
	for (var i = 1; i < 31; i++) {
		var xy = circuit.xor(~x._bits[i], y._bits[i]);
		borrow[i] = circuit.or(circuit.and(borrow[i - 1], xy), circuit.and(~x._bits[i], y._bits[i]));
	}
	if (signed) {
		var xy = circuit.xor(x._bits[31], ~y._bits[31]);
		borrow[31] = circuit.or(circuit.and(borrow[30], xy), circuit.and(x._bits[31], ~y._bits[31]));
	}
	else {
		var xy = circuit.xor(~x._bits[31], y._bits[31]);
		borrow[31] = circuit.or(circuit.and(borrow[30], xy), circuit.and(~x._bits[31], y._bits[31]));
	}
	var b = borrow[31];
	for (var i = 0; i < 32; i++)
		borrow[i] = b;
	return new CFunction(borrow, circuit.or(x._divideError, y._divideError));
}

CFunction.gt = function(x, y, signed) {
	return CFunction.lt(y, x, signed);
}

CFunction.ge = function(x, y, signed) {
	return CFunction.not(CFunction.lt(x, y, signed));
}

CFunction.le = function(x, y, signed) {
	return CFunction.not(CFunction.lt(y, x, signed));
}

CFunction.nthbit = function(x, n) {
	var bits = new Int32Array(32);
	for (var i = 0; i < 32; i++)
		bits[i] = x._bits[n];
	return new CFunction(bits, x._divideError);
}

CFunction.shlc = function(x, n) {
	var bits = new Int32Array(32);
	for (var i = n; i < 32; i++)
		bits[i] = x._bits[i - n];
	return new CFunction(bits, x._divideError);
}

CFunction.shl = function(x, y) {
	for (var i = 0; i < 5; i++) {
		var mask = CFunction.nthbit(y, i);
		var s = CFunction.shlc(x, 1 << i);
		x = CFunction.mux(mask, x, s);
	}
	return x;
}

CFunction.shruc = function(x, n) {
	var bits = new Int32Array(32);
	for (var i = 0; i < 32 - n; i++)
		bits[i] = x._bits[i + n];
	return new CFunction(bits, x._divideError);
}

CFunction.shru = function(x, y) {
	for (var i = 0; i < 5; i++) {
		var mask = CFunction.nthbit(y, i);
		var s = CFunction.shruc(x, 1 << i);
		x = CFunction.mux(mask, x, s);
	}
	return x;
}

CFunction.shrsc = function(x, n) {
	var bits = new Int32Array(32);
	for (var i = 0; i < 32; i++)
		bits[i] = i + n < 32 ? x._bits[i + n] : x._bits[31];
	return new CFunction(bits, x._divideError);
}

CFunction.shrs = function(x, y) {
	for (var i = 0; i < 5; i++) {
		var mask = CFunction.nthbit(y, i);
		var s = CFunction.shrsc(x, 1 << i);
		x = CFunction.mux(mask, x, s);
	}
	return x;
}

CFunction.rolc = function(x, n) {
	var bits = new Int32Array(32);
	for (var i = 0; i < 32; i++)
		bits[i] = x._bits[i - n & 31];
	return new CFunction(bits, x._divideError);
}

CFunction.rorc = function(x, n) {
	var bits = new Int32Array(32);
	for (var i = 0; i < 32; i++)
		bits[i] = x._bits[i + n & 31];
	return new CFunction(bits, x._divideError);
}

CFunction.rol = function(x, y) {
	for (var i = 0; i < 5; i++) {
		var mask = CFunction.nthbit(y, i);
		var s = CFunction.rolc(x, 1 << i);
		x = CFunction.mux(mask, x, s);
	}
	return x;
}

CFunction.ror = function(x, y) {
	for (var i = 0; i < 5; i++) {
		var mask = CFunction.nthbit(y, i);
		var s = CFunction.rorc(x, 1 << i);
		x = CFunction.mux(mask, x, s);
	}
	return x;
}

CFunction.spread = function(x) {
	var bits = new Int32Array(32);
	for (var i = 0; i < 32; i += 2)
		bits[i] = x._bits[i >> 1];
	return new CFunction(bits, x._divideError);
}

CFunction.mul = function(x, y) {
	var r = CFunction.constant(0);
	for (var i = 0; i < 32; i++) {
		r = CFunction.add(r, CFunction.and(x, CFunction.nthbit(y, i)));
		x = CFunction.shlc(x, 1);
	}
	return r;
}

CFunction.clmul = function(x, y) {
	var r = CFunction.constant(0);
	for (var i = 0; i < 32; i++) {
		r = CFunction.xor(r, CFunction.and(x, CFunction.nthbit(y, i)));
		x = CFunction.shlc(x, 1);
	}
	return r;
}

CFunction.clpow = function(x, y) {
	var r = CFunction.constant(1);
	var one = CFunction.constant(1);
	for (var i = 0; i < 32; i++) {
		var ith = CFunction.nthbit(y, i);
		var p = CFunction.mux(ith, one, x);
		r = CFunction.clmul(r, p);
		x = CFunction.spread(x);
	}
	return r;
}

CFunction.ormul = function(x, y) {
	var r = CFunction.constant(0);
	for (var i = 0; i < 32; i++) {
		r = CFunction.or(r, CFunction.and(x, CFunction.nthbit(y, i)));
		x = CFunction.shlc(x, 1);
	}
	return r;
}

CFunction.ez80mlt = function(x) {
	var a = CFunction.and(x, CFunction.constant(0xFF));
	var b = CFunction.and(CFunction.shruc(x, 8), CFunction.constant(0xFF));
	return CFunction.mul(a, b);
}

CFunction.abs = function(x) {
	var m = CFunction.nthbit(x, 31);
	return CFunction.xor(CFunction.add(x, m), m);
}

CFunction.eqz = function (x) {
	var t = CFunction.hor(x);
	return ~t._bits[0];
}

CFunction.xorshuf = function (x, k) {
	var bits = new Int32Array(32);
	for (var i = 0; i < 32; i++)
		bits[i] = x._bits[i ^ k];
	return new CFunction(bits, x._divideError);
}

CFunction.grev = function (x, k) {
	var r = x;
	r = CFunction.mux(CFunction.nthbit(k, 0), r, CFunction.xorshuf(r, 1));
	r = CFunction.mux(CFunction.nthbit(k, 1), r, CFunction.xorshuf(r, 2));
	r = CFunction.mux(CFunction.nthbit(k, 2), r, CFunction.xorshuf(r, 4));
	r = CFunction.mux(CFunction.nthbit(k, 3), r, CFunction.xorshuf(r, 8));
	r = CFunction.mux(CFunction.nthbit(k, 4), r, CFunction.xorshuf(r, 16));
	return r;
}

CFunction.grevmul = function (x, y) {
	var r = CFunction.constant(0);
	for (var i = 0; i < 32; i++) {
		var grevxi = CFunction.grev(x, CFunction.constant(i));
		r = CFunction.xor(r, CFunction.and(grevxi, CFunction.nthbit(y, i)));
	}
	return r;
}

CFunction.divu = function (a, b) {
	var diverror = circuit.or(CFunction.eqz(b), circuit.or(a._divideError, b._divideError));
	var P = new Int32Array(64);
	for (var i = 0; i < 32; i++)
		P[i] = a._bits[i];
	var D = new Int32Array(64);
	for (var i = 0; i < 32; i++)
		D[i + 32] = b._bits[i];
	var bits = new Int32Array(32);

	for (var i = 31; i >= 0; i--) {
		for (var j = P.length - 1; j > 0; j--)
			P[j] = P[j - 1];
		P[0] = 0;
		var borrow = new Int32Array(64);
		var newP = new Int32Array(64);
		for (var j = 0; j < P.length; j++) {
			var ab = circuit.xor(P[j], D[j]);
			newP[j] = ab;
			if (j > 0) {
				newP[j] = circuit.xor(newP[j], borrow[j - 1]);
				borrow[j] = circuit.or(circuit.and(~ab, borrow[j - 1]), circuit.and(~P[j], D[j]));
			}
			else
				borrow[j] = circuit.and(~P[j], D[j]);
		}
		bits[i] = ~borrow[63];
		if (i != 0) {
			for (var j = 63; j > 0; j--)
				P[j] = circuit.or(circuit.and(newP[j], ~borrow[63]), circuit.and(P[j], borrow[63]));
		}
	}
	return new CFunction(bits, diverror);
}

CFunction.divs = function (a, b) {
	var sign = CFunction.xor(CFunction.nthbit(a, 31), CFunction.nthbit(b, 31));
	var div = CFunction.divu(CFunction.abs(a), CFunction.abs(b));
	return CFunction.xor(sign, CFunction.add(sign, div));
}

CFunction.dive = function (a, b) {
	var sa = CFunction.nthbit(a, 31);
	var sb = CFunction.nthbit(b, 31);
	var div = CFunction.divu(CFunction.xor(a, sa), CFunction.abs(b));
	return CFunction.xor(sb, CFunction.add(sb, CFunction.xor(sa, div)));
}

CFunction.divupony = function (a, b) {
	var bnz = CFunction.hor(b);
	var az = CFunction.and(a, bnz);
	return new CFunction(CFunction.divu(az, CFunction.or(b, CFunction.not(bnz)))._bits, 0);
}

CFunction.remu = function (a, b) {
	var diverror = circuit.or(CFunction.eqz(b), circuit.or(a._divideError, b._divideError));
	var P = new Int32Array(64);
	for (var i = 0; i < 32; i++)
		P[i] = a._bits[i];
	var D = new Int32Array(64);
	for (var i = 0; i < 32; i++)
		D[i + 32] = b._bits[i];
	var bits = new Int32Array(32);

	for (var i = 31; i >= 0; i--) {
		for (var j = P.length - 1; j > 0; j--)
			P[j] = P[j - 1];
		P[0] = 0;
		var borrow = new Int32Array(64);
		var newP = new Int32Array(64);
		for (var j = 0; j < P.length; j++) {
			var ab = circuit.xor(P[j], D[j]);
			newP[j] = ab;
			if (j > 0) {
				newP[j] = circuit.xor(newP[j], borrow[j - 1]);
				borrow[j] = circuit.or(circuit.and(~ab, borrow[j - 1]), circuit.and(~P[j], D[j]));
			}
			else
				borrow[j] = circuit.and(~P[j], D[j]);
		}
		for (var j = 63; j > 0; j--)
			P[j] = circuit.or(circuit.and(newP[j], ~borrow[63]), circuit.and(P[j], borrow[63]));
	}

	for (var i = 0; i < 32; i++)
		bits[i] = P[i + 32];
	return new CFunction(bits, diverror);
}

CFunction.rems = function (a, b) {
	var sign = CFunction.xor(CFunction.nthbit(a, 31), CFunction.nthbit(b, 31));
	var div = CFunction.remu(CFunction.abs(a), CFunction.abs(b));
	return CFunction.xor(sign, CFunction.add(sign, div));
}

CFunction.reme = function (a, b) {
	var sa = CFunction.nthbit(a, 31);
	b = CFunction.abs(b);
	var div = CFunction.remu(CFunction.xor(a, sa), b);
	return CFunction.add(CFunction.and(sa, b), CFunction.xor(sa, div));
}

function cf_mul64(a, b, signed) {
	var c = new Int32Array(64);
	var a_sh = new Int32Array(64);
	for (var i = 0; i < 32; i++)
		a_sh[i] = a[i];
	for (var j = 0; j < 32; j++) {
		var m = b[j];
		if (m != 0) {
			var carry = 0;
			for (var i = 0; i < 64; i++) {
				var am = circuit.and(m, a_sh[i]);
				var ac = circuit.and(am, c[i]);
				var aoc = circuit.or(am, c[i]);
				var acc = circuit.and(ac, carry);
				var nacc = circuit.and(~aoc, ~carry);
				carry = circuit.or(circuit.and(carry, aoc), ac);
				c[i] = circuit.and(circuit.or(~carry, acc), ~nacc);
			}
		}
		for (var i = 63; i > 0; i--)
			a_sh[i] = a_sh[i - 1];
		a_sh[0] = 0;
	}
	if (signed) {
		var bfa = new CFunction(a, 0);
		var bfb = new CFunction(b, 0);
		var t1 = CFunction.and(bfa, CFunction.nthbit(bfb, 31));
		var t2 = CFunction.and(bfb, CFunction.nthbit(bfa, 31));
		var tophalf = new Int32Array(32);
		for (var i = 0; i < 32; i++)
			tophalf[i] = c[i + 32];
		var x = new CFunction(tophalf, 0);
		x = CFunction.sub(x, CFunction.add(t1, t2));
		for (var i = 0; i < 32; i++)
			c[i + 32] = x._bits[i];
	}
	return c;
};

CFunction.hmul = function(x, y, signed) {
	var prod = cf_mul64(x._bits, y._bits, signed);
	var bits = new Int32Array(32);
	for (var i = 0; i < 32; i++)
		bits[i] = prod[i + 32];
	return new CFunction(bits, circuit.or(x._divideError, y._divideError));
};

CFunction.fixscale = function(x, n, d) {
	// FIXME
	var prod = cf_mul64(x._bits, n._bits, false);
	var diverror = circuit.or(CFunction.eqz(d), circuit.or(circuit.or(x._divideError, n._divideError), d._divideError));
	var P = new Int32Array(128);
	for (var i = 0; i < 64; i++)
		P[i] = prod[i];
	var D = new Int32Array(128);
	for (var i = 0; i < 32; i++)
		D[i + 64] = d._bits[i];
	var bits = new Int32Array(32);

	for (var i = 63; i >= 0; i--) {
		for (var j = P.length - 1; j > 0; j--)
			P[j] = P[j - 1];
		P[0] = 0;
		var borrow = new Int32Array(128);
		var newP = new Int32Array(128);
		for (var j = 0; j < P.length; j++) {
			var ab = circuit.xor(P[j], D[j]);
			newP[j] = ab;
			if (j > 0) {
				newP[j] = circuit.xor(newP[j], borrow[j - 1]);
				borrow[j] = circuit.or(circuit.and(~ab, borrow[j - 1]), circuit.and(~P[j], D[j]));
			}
			else
				borrow[j] = circuit.and(~P[j], D[j]);
		}
		for (var j = 127; j > 0; j--)
			P[j] = circuit.or(circuit.and(newP[j], ~borrow[127]), circuit.and(P[j], borrow[127]));
	}

	for (var i = 0; i < 32; i++)
		bits[i] = P[i + 64];
	return new CFunction(bits, diverror);
};

CFunction.ctz = function (x) {
	x = CFunction.and(CFunction.not(x), CFunction.add(x, CFunction.constant(-1)));
	return CFunction.popcnt(x);
};

CFunction.clz = function (x) {
	x = CFunction.or(x, CFunction.shruc(x, 1));
	x = CFunction.or(x, CFunction.shruc(x, 2));
	x = CFunction.or(x, CFunction.shruc(x, 4));
	x = CFunction.or(x, CFunction.shruc(x, 8));
	x = CFunction.or(x, CFunction.shruc(x, 16));
	return CFunction.popcnt(CFunction.not(x));
};

CFunction.popcnt = function(x) {
	var bits = [[]];
	for (var i = 0; i < 32; i++)
		bits[0].push(x._bits[i]);
	while (!bits[5]) {
		var i = 0;
		while (i < 5 && bits[i].length < 3) i++;
		if (i < 5) {
			var b1 = bits[i].pop();
			var b2 = bits[i].pop();
			var b3 = bits[i].pop();
			bits[i].unshift(circuit.xor(circuit.xor(b1, b2), b3));
			if (!bits[i + 1]) bits[i + 1] = [];
			bits[i + 1].unshift(circuit.carry(b1, b2, b3));
		}
		else {
			i = 0;
			while (i < 5 && bits[i].length < 2) i++;
			var b1 = bits[i].pop();
			var b2 = bits[i].pop();
			bits[i].unshift(circuit.xor(b1, b2));
			if (!bits[i + 1]) bits[i + 1] = [];
			bits[i + 1].unshift(circuit.and(b1, b2));
		}
	}
	var r = new Int32Array(32);
	r[0] = bits[0][0];
	r[1] = bits[1][0];
	r[2] = bits[2][0];
	r[3] = bits[3][0];
	r[4] = bits[4][0];
	r[5] = circuit.and_big(x._bits.slice());
	return new CFunction(r, x._divideError);
};

CFunction.popcnt2 = function(x) {
	var one = CFunction.constant(1);
	var r = CFunction.shruc(x, 31);
	for (var i = 30; i >= 0; i--) {
		r = CFunction.add(r, CFunction.and(CFunction.shruc(x, i), one));
	}
	return r;
};

CFunction.pdep = function (value, mask) {
	var res = CFunction.constant(0);
	for (var i = 0; i < 32; i++) {
		var lowest = CFunction.and(CFunction.sub(CFunction.constant(0), mask), mask);
		mask = CFunction.and(mask, CFunction.not(lowest));
		var vbit = CFunction.nthbit(value, i);
		res = CFunction.or(res, CFunction.and(lowest, vbit));
	}
	return res;
};

CFunction.pext = function (value, mask) {
	var res = CFunction.constant(0);
	for (var i = 0; i < 32; i++) {
		var lowest = CFunction.and(CFunction.sub(CFunction.constant(0), mask), mask);
		mask = CFunction.and(mask, CFunction.not(lowest));
		var spread = CFunction.hor(CFunction.and(lowest, value));
		var biti = CFunction.constant(1 << i);
		res = CFunction.or(res, CFunction.and(biti, spread));
	}
	return res;
};

CFunction.gf2affine = function (x, m1, m0) {
	function repbyte(x, k) {
		var t = CFunction.shruc(CFunction.shlc(x, k * 8), 24);
		t = CFunction.or(t, CFunction.shlc(t, 8));
		t = CFunction.or(t, CFunction.shlc(t, 16));
		return t;
	}
	var bytes = [];
	bytes.push(repbyte(m1, 0));
	bytes.push(repbyte(m1, 1));
	bytes.push(repbyte(m1, 2));
	bytes.push(repbyte(m1, 3));
	bytes.push(repbyte(m0, 0));
	bytes.push(repbyte(m0, 1));
	bytes.push(repbyte(m0, 2));
	bytes.push(repbyte(m0, 3));

	var res = CFunction.constant(0);
	var m = CFunction.constant(0x01010101);
	for (var i = 0; i < 8; i++) {
		var t = CFunction.and(x, bytes[i]);
		t = CFunction.xor(t, CFunction.shruc(t, 4));
		t = CFunction.xor(t, CFunction.shruc(t, 2));
		t = CFunction.xor(t, CFunction.shruc(t, 1));
		t = CFunction.and(t, m);
		res = CFunction.or(res, CFunction.shlc(t, i));
	}
	return res;
};

CFunction.ternlog = function (a, b, c, lut) {
	let res = CFunction.constant(0);

	let na = CFunction.not(a);
	let nb = CFunction.not(b);
	let nc = CFunction.not(c);

	res = CFunction.or(res, CFunction.and(CFunction.and(na, nb), CFunction.and(nc, CFunction.nthbit(lut, 0))));
	res = CFunction.or(res, CFunction.and(CFunction.and(na, nb), CFunction.and(c, CFunction.nthbit(lut, 1))));
	res = CFunction.or(res, CFunction.and(CFunction.and(na, b), CFunction.and(nc, CFunction.nthbit(lut, 2))));
	res = CFunction.or(res, CFunction.and(CFunction.and(na, b), CFunction.and(c, CFunction.nthbit(lut, 3))));
	res = CFunction.or(res, CFunction.and(CFunction.and(a, nb), CFunction.and(nc, CFunction.nthbit(lut, 4))));
	res = CFunction.or(res, CFunction.and(CFunction.and(a, nb), CFunction.and(c, CFunction.nthbit(lut, 5))));
	res = CFunction.or(res, CFunction.and(CFunction.and(a, b), CFunction.and(nc, CFunction.nthbit(lut, 6))));
	res = CFunction.or(res, CFunction.and(CFunction.and(a, b), CFunction.and(c, CFunction.nthbit(lut, 7))));

	return res;
};

CFunction.ternlogb = function (a, b, c, lut) {
	let bits = new Int32Array(32);

	for (let i = 0; i < 32; i++) {
		let r = 0;
		r = circuit.or(r, circuit.and(circuit.and(~a._bits[i], ~b._bits[i]), circuit.and(~c._bits[i], lut._bits[(i & 0x18) + 0])));
		r = circuit.or(r, circuit.and(circuit.and(~a._bits[i], ~b._bits[i]), circuit.and( c._bits[i], lut._bits[(i & 0x18) + 1])));
		r = circuit.or(r, circuit.and(circuit.and(~a._bits[i],  b._bits[i]), circuit.and(~c._bits[i], lut._bits[(i & 0x18) + 2])));
		r = circuit.or(r, circuit.and(circuit.and(~a._bits[i],  b._bits[i]), circuit.and( c._bits[i], lut._bits[(i & 0x18) + 3])));
		r = circuit.or(r, circuit.and(circuit.and( a._bits[i], ~b._bits[i]), circuit.and(~c._bits[i], lut._bits[(i & 0x18) + 4])));
		r = circuit.or(r, circuit.and(circuit.and( a._bits[i], ~b._bits[i]), circuit.and( c._bits[i], lut._bits[(i & 0x18) + 5])));
		r = circuit.or(r, circuit.and(circuit.and( a._bits[i],  b._bits[i]), circuit.and(~c._bits[i], lut._bits[(i & 0x18) + 6])));
		r = circuit.or(r, circuit.and(circuit.and( a._bits[i],  b._bits[i]), circuit.and( c._bits[i], lut._bits[(i & 0x18) + 7])));
		bits[i] = r;
	}

	let diverror = circuit.or(circuit.or(a._divideError, b._divideError), circuit.or(c._divideError, lut._divideError));
	return new CFunction(bits, diverror);
};

CFunction.prototype.sat = function(varcount) {
	if (!varcount)
		varcount = 4;
	if (this._bits[0] == 0)
		return null;
	var sat = new SAT();
	circuit.to_cnf(this._bits[0], sat);
	var res = sat.solve();
	if (res) {		
		var values = new Int32Array(varcount);
		for (var i = 0; i < varcount * 32; i++) {
			if (res[i + 1] == 1)
				values[i >> 5] |= 1 << (i & 31);
		}
		return values;
	}
	return null;
};

CFunction.AnalyzeTruthQ = function(data, root, vars, quantified, cb) {
	let res = data;
	res.msg = "Using SAT fallback";
	cb();

	function getModel(expr, counterExamples, bannedModels, cb) {
		var sat = new SAT();
		counterExamples.forEach((ce) => {
			let M = [];
			for (let i = 0; i < vars.length; i++) {
				if (quantified.indexOf(vars[i]) >= 0)
					M[i] = new Constant(ce[i]);
			}
			let concrete = Node.ReplaceVariables(expr, M);
			let cf = concrete.toCircuitFunc();
			circuit.to_cnf(cf._bits[0], sat);
		});
		bannedModels.forEach(function (bannedModel) {
			var clause = [];
			for (var i = 0; i < 32 * vars.length; i++) {
				if (quantified.indexOf(vars[i >> 5]) >= 0)
					continue;
				if ((bannedModel[i >> 5] & (1 << (i & 31))) == 0)
					clause.push(i + 1);
				else
					clause.push(~(i + 1));
			}
			sat.addClause(clause);
		});
		sat.solve(function (raw) {
			if (!raw) return cb(null);
			var model = new Int32Array(64);
			for (var i = 1; i <= 32 * 64; i++) {
				if (raw[i] == 1)
					model[(i - 1) >> 5] |= 1 << ((i - 1) & 31);
			}
			cb(model);
		});
	}

	function getCE(expr, model, quantified, vars, cb) {
		var sat = new SAT();
		let M = [];
		for (let i = 0; i < vars.length; i++) {
			if (quantified.indexOf(vars[i]) < 0)
				M[i] = new Constant(model[i]);
		}
		let syn = Node.ReplaceVariables(expr, M);
		let cf = syn.toCircuitFunc();
		circuit.to_cnf(~cf._bits[0], sat);
		sat.solve(function (raw) {
			if (!raw) return cb(null);
			var model = new Int32Array(64);
			for (var i = 1; i <= 32 * 64; i++) {
				if (raw[i] == 1)
					model[(i - 1) >> 5] |= 1 << ((i - 1) & 31);
			}
			cb(model);
		});
	}

	function Int32ArrayEquals (a, b) {
		if (a.length != b.length)
			return false;
		for (var i = 0; i < a.length; i++) {
			if (a[i] != b[i])
				return false;
		}
		return true;
	}

	function theloop(res, expr, vars, quantified, cb, counterExamples, bannedModels) {
		if (counterExamples.length > 50) {
			return;
		}

		getModel(expr, counterExamples, bannedModels, (model) => {
			console.log("model: ", model);
			if (!model) {
				if (bannedModels.length == 0) {
					res.false = {
						count: "#always"
					};
				}
				else if (bannedModels.length == 1 && res.true) {
					res.true.count = 1n;
				}
				return cb();
			}

			getCE(expr, model, quantified, vars, (ce) => {
				console.log("CE: ", ce);
				if (!ce) {
					if (bannedModels.length == 0) {
						res.true = {
							count: "#at least once",
							examples: function(ix) {
								return [model][ix];
							}
						};
						cb();
						bannedModels.push(model);
						window.setTimeout(theloop, 0, res, expr, vars, quantified, cb, counterExamples, bannedModels);
						return;
					}
					else
						return;
				}
				if (counterExamples.some((a) => Int32ArrayEquals(a, ce))) {
					debugger
					alert("bad counter example");
					return;
				}
				counterExamples.push(ce);
				window.setTimeout(theloop, 0, res, expr, vars, quantified, cb, counterExamples, bannedModels);
			});
		});
	}

	window.setTimeout(theloop, 0, res, root, vars, quantified, cb, [new Int32Array(vars.length)], []);
};

CFunction.prototype.AnalyzeTruth = function(data, root, vars, callback) {
	var res = data;
	res.msg = "Using SAT fallback";

	function getModel(bit, cb, bannedModels) {
		if (!bannedModels) bannedModels = [];
		else if (bannedModels[0] == null) return cb(null);
		if (bit == 0) return cb(null);
		if (bit == -1 && bannedModels.length == 0) return cb(new Int32Array(64));
		var sat = new SAT();
		circuit.to_cnf(bit, sat);
		bannedModels.forEach(function (bannedModel) {
			var clause = [];
			for (var i = 0; i < 32 * vars.length; i++) {
				if ((bannedModel[i >> 5] & (1 << (i & 31))) == 0)
					clause.push(i + 1);
				else
					clause.push(~(i + 1));
			}
			sat.addClause(clause);
		});
		sat.solve(function (raw) {
			if (!raw) return cb(null);
			var model = new Int32Array(64);
			for (var i = 1; i <= 32 * 64; i++) {
				if (raw[i] == 1)
					model[(i - 1) >> 5] |= 1 << ((i - 1) & 31);
			}
			cb(model);
		});
	}

	var true_without_de = circuit.and(~this._divideError, this._bits[0]);
	var false_without_de = circuit.and(~this._divideError, ~this._bits[0]);
	var divideError = this._divideError;
	getModel(~divideError, function (ndemodel) {
	getModel(divideError, function (demodel) {
	if (demodel) {
		res.diverror = {
			count: "#at least once",
			examples: function(ix) {
				return [demodel][ix];
			}
		};
		if (!ndemodel) res.diverror.count = "#always";
		callback();
	}
	getModel(true_without_de, function (tmodel) {
	if (tmodel) {
		res.true = {
			count: "#at least once",
			examples: function(ix) {
				return [tmodel][ix];
			}
		};
		callback();
	}
	getModel(false_without_de, function (fmodel) {
	if (fmodel) {
		res.false = {
			count: "#at least once",
			examples: function(ix) {
				return [fmodel][ix];
			}
		};
		callback();
	}

	if (!demodel && !fmodel && tmodel) {
		var resobj = {count: "#always"};
		if (root.type == 'bin' && (root.op == 20 || root.op == 41 || root.op == 45)) {
			ProofFinder.proveAsync(root.l, root.r, root.op, function (flatproof) {
				resobj.proof = flatproof;
				callback();
			});
		}
		res.true = resobj;
		callback();
		return;
	}
	if (!demodel && !tmodel && fmodel) {
		res.false = {count: "#always"};
		callback();
		return;
	}
	if (!tmodel && !fmodel && demodel) {
		res.diverror = {count: "#always"};
		callback();
		return;
	}


	// second set of models
	getModel(true_without_de, function (tmodel2) {
	if (tmodel2) {
		res.true = {
			count: "#at least twice",
			examples: function(ix) {
				return [tmodel, tmodel2][ix];
			}
		};
		callback();
	}
	else if (tmodel) {
		res.true.count = 1n;
		callback();
	}
	getModel(false_without_de, function (fmodel2) {
	if (fmodel2) {
		res.false = {
			count: "#at least twice",
			examples: function(ix) {
				return [fmodel, fmodel2][ix];
			}
		};
		callback();
	}
	else if (fmodel) {
		res.false.count = 1n;
		callback();
	}
	}, [fmodel]);
	}, [tmodel]);

	});
	});
	});
	});

	return null;
}